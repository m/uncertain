from .uncertain import UncertainValue, probability_in_interval, from_data

__version__ = "0.0.10"
